# SFTP server

A chrooted SFTP server separate from the administrative SSHD service.

## Dependencies

    $ ansible-galaxy install -r requirements.yml

## Run in vagrant

This is equivalent of running ``ansible-playbook -i inventory/default/hosts bootstrap.yml

    $ vagrant up
    $ vagrant ssh-config > ~/.ssh/vagrant.conf

This sets up users and potentially changes your management SSHD to run on a different port.

    $ ansible-playbook -i inventory/default/hosts site.yml

## TODO

* Fix SElinux policy so we can run in Enforcing mode
